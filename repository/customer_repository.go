package repository

import (
	modelCustomerVisit "gitlab.com/VNEU/visit-flow-go/model/domain"
	goHelper "gitlab.com/vneu/go-helper/helper"
)

type CustomerRepository interface {
	FindAllCustomerSki(db *goHelper.DatabaseResolver, updateAt, customer string) modelCustomerVisit.Customers
}
