package repository

import (
	"gitlab.com/VNEU/sync-visit-ski/helper"
	modelLocationVisit "gitlab.com/VNEU/visit-flow-go/model/domain"
	goHelper "gitlab.com/vneu/go-helper/helper"
)

type LocationRepositoryImpl struct {
}

func NewLocationRepository() LocationRepository {
	return &LocationRepositoryImpl{}
}

func (repository *LocationRepositoryImpl) FindAllLocationSki(db *goHelper.DatabaseResolver, updateAt, location string) modelLocationVisit.Locations {
	locations := modelLocationVisit.Locations{}
	tx := db.Read.Select(`id,
		created_by_id,
		created_at,
		updated_at,
		updated_by_id,
		deleted_at,
		deleted_by_id,
		name,
		city_id AS area_id,
		latitude,
		longitude,
		address,
		id AS no_location_by_company,
		marketing_location_boss_id AS boss_code,
		1 AS company_id,
		'approve' AS status
	`).
		Table("ski_production.outlets").
		Where("LEFT(updated_at,7) = ?", updateAt)

	if location != "ALL" {
		tx = tx.Where("id = ?", location)
	}

	err := tx.Find(&locations).Error
	helper.PanicIfError(err)

	return locations
}
