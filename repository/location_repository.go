package repository

import (
	modelLocationVisit "gitlab.com/VNEU/visit-flow-go/model/domain"
	goHelper "gitlab.com/vneu/go-helper/helper"
)

type LocationRepository interface {
	FindAllLocationSki(db *goHelper.DatabaseResolver, updateAt, location string) modelLocationVisit.Locations
}
