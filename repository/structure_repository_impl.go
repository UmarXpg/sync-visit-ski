package repository

import (
	"gitlab.com/VNEU/sync-visit-ski/helper"
	modelStructureVisit "gitlab.com/VNEU/visit-flow-go/model/domain"
	goHelper "gitlab.com/vneu/go-helper/helper"
)

type StructureRepositoryImpl struct {
}

func NewStructureRepository() StructureRepository {
	return &StructureRepositoryImpl{}
}

func (repository *StructureRepositoryImpl) FindAllStructureSki(db *goHelper.DatabaseResolver, updateAt, structure string) modelStructureVisit.Structures {
	structures := modelStructureVisit.Structures{}
	tx := db.Read.Select(`code AS id,
		created_by_id,
		created_at,
		updated_at,
		updated_by_id,
		deleted_at,
		deleted_by_id,
		user_id,
		period,
		CASE WHEN marketing_position_id = "MR" THEN 1 
			WHEN marketing_position_id = "SPV" THEN 2 
			WHEN marketing_position_id = "AM" THEN 3 
			WHEN marketing_position_id = "FSM" THEN 4 
			WHEN marketing_position_id = "GM" THEN 5 
			WHEN marketing_position_id = "MD" THEN 6 
		ELSE NULL END AS level,
		marketing_structure_boss_id AS boss_code,
		1 AS company_id
	`).
		Table("ski_production.marketing_structures").
		Where("period = ?", updateAt)

	if structure != "ALL" {
		tx = tx.Where("code = ?", structure)
	}

	err := tx.Find(&structures).Error
	helper.PanicIfError(err)

	return structures
}
