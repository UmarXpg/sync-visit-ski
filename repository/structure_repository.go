package repository

import (
	modelStructureVisit "gitlab.com/VNEU/visit-flow-go/model/domain"
	goHelper "gitlab.com/vneu/go-helper/helper"
)

type StructureRepository interface {
	FindAllStructureSki(db *goHelper.DatabaseResolver, updateAt, structure string) modelStructureVisit.Structures
}
