package repository

import (
	"gitlab.com/VNEU/sync-visit-ski/helper"
	modelCustomerVisit "gitlab.com/VNEU/visit-flow-go/model/domain"
	goHelper "gitlab.com/vneu/go-helper/helper"
)

type CustomerRepositoryImpl struct {
}

func NewCustomerRepository() CustomerRepository {
	return &CustomerRepositoryImpl{}
}

func (repository *CustomerRepositoryImpl) FindAllCustomerSki(db *goHelper.DatabaseResolver, updateAt, customer string) modelCustomerVisit.Customers {
	customers := modelCustomerVisit.Customers{}
	tx := db.Read.Select(`id,
		created_by_id,
		created_at,
		updated_at,
		updated_by_id,
		deleted_at,
		deleted_by_id,
		name,
		phone,
		email,
		id AS customer_id_by_company,
		latitude,
		longitude,
		address,
		CASE WHEN gender = 'Laki-laki' THEN 'famele'
		WHEN gender = 'Perempuan' THEN 'male' ELSE NULL END AS gender,
		1 AS company_id,
		'approve' AS status
	`).
		Table("ski_production.customers").
		Where("LEFT(updated_at,7) = ?", updateAt)

	if customer != "ALL" {
		tx = tx.Where("id = ?", customer)
	}

	err := tx.Find(&customers).Error
	helper.PanicIfError(err)

	return customers
}
