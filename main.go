package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/go-playground/validator/v10"
	"gitlab.com/VNEU/sync-visit-ski/app"

	goHelper "gitlab.com/vneu/go-helper/helper"
)

func main() {
	configuration, err := goHelper.LoadConfig()
	if err != nil {
		log.Fatalln("Failed at config", err)
	}
	fmt.Println(configuration.Port)
	port := configuration.Port
	db := app.ConnectDatabase(configuration)

	// Validator
	validate := validator.New()

	router := app.NewRouter(db, validate)
	server := http.Server{
		Addr:    ":" + port,
		Handler: router,
	}
	log.Printf("Server is running on port %s", port)

	err = server.ListenAndServe()
	goHelper.PanicIfError(err)
}
