package service

import (
	"fmt"
	"time"

	"gitlab.com/VNEU/sync-visit-ski/repository"
	modelCustomerVisit "gitlab.com/VNEU/visit-flow-go/model/domain"
	customerVisit "gitlab.com/VNEU/visit-flow-go/repository"
	goHelper "gitlab.com/vneu/go-helper/helper"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"gorm.io/gorm"
)

type CustomerServiceImpl struct {
	CustomerSkiRepository   repository.CustomerRepository
	CustomerVisitRepository customerVisit.CustomerRepository
	DB                      *gorm.DB
	Validate                *validator.Validate
}

func NewCustomerService(
	customerSki repository.CustomerRepository,
	customerVisit customerVisit.CustomerRepository,
	db *gorm.DB,
	validate *validator.Validate,
) CustomerService {
	return &CustomerServiceImpl{
		CustomerSkiRepository:   customerSki,
		CustomerVisitRepository: customerVisit,
		DB:                      db,
		Validate:                validate,
	}
}

func (service *CustomerServiceImpl) SyncCustomer(Customer string, c *gin.Context) {
	goHelper.SignozSpan("Customer_sync_ski", c)

	tx := goHelper.CreateTransaction(service.DB, c)
	defer goHelper.CommitOrRollback(tx.Write)

	formattedTime := time.Now().Format("2006-01")

	customerSki := service.CustomerSkiRepository.FindAllCustomerSki(tx, formattedTime, Customer)

	for _, data := range customerSki {
		companyID := uint(1)
		customerVisit := service.CustomerVisitRepository.FindByID(tx, &data.ID, &data.CompanyID)
		dataCustomerUpdate := &modelCustomerVisit.Customer{
			ID:          data.ID,
			UpdatedByID: data.UpdatedByID,
			CreatedByID: data.CreatedByID,
			UpdatedAt:   data.UpdatedAt,
			CreatedAt:   data.CreatedAt,
			DeletedByID: data.DeletedByID,
			DeletedAt:   data.DeletedAt,
			Name:        data.Name,
			Address:     data.Address,
			CompanyID:   data.CompanyID,
			Status:      data.Status,
		}

		if customerVisit.ID == "" {
			service.CustomerVisitRepository.Create(tx, dataCustomerUpdate)
		} else {
			service.CustomerVisitRepository.Update(tx, dataCustomerUpdate, &data.ID, &companyID)
		}
	}
	fmt.Println(formattedTime)

}
