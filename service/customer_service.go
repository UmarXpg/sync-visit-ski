package service

import (
	"github.com/gin-gonic/gin"
)

type CustomerService interface {
	SyncCustomer(customer string, c *gin.Context)
}
