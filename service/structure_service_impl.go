package service

import (
	"time"

	"gitlab.com/VNEU/sync-visit-ski/repository"
	modelStructureVisit "gitlab.com/VNEU/visit-flow-go/model/domain"
	structureVisit "gitlab.com/VNEU/visit-flow-go/repository"
	goHelper "gitlab.com/vneu/go-helper/helper"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"gorm.io/gorm"
)

type StructureServiceImpl struct {
	StructureSkiRepository   repository.StructureRepository
	StructureVisitRepository structureVisit.StructureRepository
	DB                       *gorm.DB
	Validate                 *validator.Validate
}

func NewStructureService(
	structureSki repository.StructureRepository,
	structureVisit structureVisit.StructureRepository,
	db *gorm.DB,
	validate *validator.Validate,
) StructureService {
	return &StructureServiceImpl{
		StructureSkiRepository:   structureSki,
		StructureVisitRepository: structureVisit,
		DB:                       db,
		Validate:                 validate,
	}
}

func (service *StructureServiceImpl) SyncStructure(structure string, c *gin.Context) {
	goHelper.SignozSpan("structure_sync_ski", c)

	tx := goHelper.CreateTransaction(service.DB, c)
	defer goHelper.CommitOrRollback(tx.Write)

	// formattedTime := time.Now().Format("2006-01")
	period := time.Now().Format("200601")

	structureSki := service.StructureSkiRepository.FindAllStructureSki(tx, period, structure)

	for _, data := range structureSki {
		structureVisit := service.StructureVisitRepository.FindByPeriod(tx, &data.ID, &period)
		dataStructureUpdate := &modelStructureVisit.Structure{
			Model: gorm.Model{
				DeletedAt: data.DeletedAt,
				UpdatedAt: data.UpdatedAt,
				CreatedAt: data.CreatedAt,
			},
			ID:          data.ID,
			UpdatedByID: data.UpdatedByID,
			CreatedByID: data.CreatedByID,
			DeletedByID: data.DeletedByID,
			Period:      period,
			BossCode:    (data.BossCode)[6:],
			CompanyID:   data.CompanyID,
			Level:       data.Level,
		}

		if data.UserID != 0 {
			dataStructureUpdate.UserID = data.UserID
		}

		if structureVisit.ID == "" {
			service.StructureVisitRepository.Create(tx, dataStructureUpdate)
		} else {
			service.StructureVisitRepository.UpdateByPeriod(tx, dataStructureUpdate, &data.ID, &period)
		}
	}
	// fmt.Println(formattedTime)

}
