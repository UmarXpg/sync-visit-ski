package service

import (
	"fmt"
	"time"

	"gitlab.com/VNEU/sync-visit-ski/repository"
	modelLocationVisit "gitlab.com/VNEU/visit-flow-go/model/domain"
	LocationVisit "gitlab.com/VNEU/visit-flow-go/repository"
	goHelper "gitlab.com/vneu/go-helper/helper"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"gorm.io/gorm"
)

type LocationServiceImpl struct {
	LocationSkiRepository   repository.LocationRepository
	LocationVisitRepository LocationVisit.LocationRepository
	DB                      *gorm.DB
	Validate                *validator.Validate
}

func NewLocationService(
	LocationSki repository.LocationRepository,
	LocationVisit LocationVisit.LocationRepository,
	db *gorm.DB,
	validate *validator.Validate,
) LocationService {
	return &LocationServiceImpl{
		LocationSkiRepository:   LocationSki,
		LocationVisitRepository: LocationVisit,
		DB:                      db,
		Validate:                validate,
	}
}

func (service *LocationServiceImpl) SyncLocation(Location string, c *gin.Context) {
	goHelper.SignozSpan("location_sync_ski", c)

	tx := goHelper.CreateTransaction(service.DB, c)
	defer goHelper.CommitOrRollback(tx.Write)

	formattedTime := time.Now().Format("2006-01")

	locationSki := service.LocationSkiRepository.FindAllLocationSki(tx, formattedTime, Location)

	for _, data := range locationSki {
		companyID := uint(1)
		locationVisit := service.LocationVisitRepository.FindByID(tx, &data.ID, &companyID)
		dataLocationUpdate := &modelLocationVisit.Location{
			ID:                  data.ID,
			UpdatedByID:         data.UpdatedByID,
			CreatedByID:         data.CreatedByID,
			UpdatedAt:           data.UpdatedAt,
			CreatedAt:           data.CreatedAt,
			DeletedByID:         data.DeletedByID,
			DeletedAt:           data.DeletedAt,
			Name:                data.Name,
			Latitude:            data.Latitude,
			Longitude:           data.Longitude,
			Address:             data.Address,
			NoLocationByCompany: data.NoLocationByCompany,
			AreaID:              data.AreaID,
			CompanyID:           data.CompanyID,
			Status:              data.Status,
		}

		if locationVisit.ID == "" {
			service.LocationVisitRepository.Create(tx, dataLocationUpdate)
		} else {
			service.LocationVisitRepository.Update(tx, dataLocationUpdate, &data.ID, &companyID)
		}
	}
	fmt.Println(formattedTime)

}
