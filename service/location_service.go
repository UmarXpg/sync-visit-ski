package service

import (
	"github.com/gin-gonic/gin"
)

type LocationService interface {
	SyncLocation(location string, c *gin.Context)
}
