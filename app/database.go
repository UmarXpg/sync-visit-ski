package app

import (
	"github.com/uptrace/opentelemetry-go-extra/otelgorm"
	goHelper "gitlab.com/vneu/go-helper/helper"
	"gorm.io/gorm"
)

func ConnectDatabase(configuration goHelper.Configuration) *gorm.DB {
	database := goHelper.ConnectMysqlDatabaseResolver(configuration)

	if err := database.Use(otelgorm.NewPlugin()); err != nil {
		panic(err)
	}

	// RUN before_auto_migrate.sql
	// helper.RunSQLFromFile(database, "app/database/before_auto_migrate.sql")

	// err := database.AutoMigrate(
	// 	// Product
	// 	&domain.Distributor{},
	// 	&domain.Material{},
	// 	&domain.OutletSurvey{},
	// 	&domain.OutletSurveyCustomer{},
	// 	&domain.OutletSurveyCustomerMaterial{},
	// 	&domain.OutletSurveyQuestion{},
	// )
	// if err != nil {
	// 	panic("failed to auto migrate schema")
	// }

	// RUN after_auto_migrate.sql
	// helper.RunSQLFromFile(database, "app/database/after_auto_migrate.sql")

	return database
}
