package controller

import (
	"net/http"

	"gitlab.com/VNEU/sync-visit-ski/model/web"
	"gitlab.com/VNEU/sync-visit-ski/service"

	"github.com/gin-gonic/gin"
)

type StructureControllerImpl struct {
	StructureService service.StructureService
}

func NewStructureController(structureService service.StructureService) StructureController {
	return &StructureControllerImpl{
		StructureService: structureService,
	}
}

func (controller *StructureControllerImpl) SyncStructure(c *gin.Context) {
	structure := c.Param("isAll")
	controller.StructureService.SyncStructure(structure, c)
	webResponse := web.WebResponse{
		Success: true,
		Message: "Sync Structure Succesfully",
	}

	c.JSON(http.StatusOK, webResponse)
}
