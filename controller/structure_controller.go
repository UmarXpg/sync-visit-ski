package controller

import (
	"github.com/gin-gonic/gin"
)

type StructureController interface {
	SyncStructure(context *gin.Context)
}
