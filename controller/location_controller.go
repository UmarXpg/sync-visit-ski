package controller

import (
	"github.com/gin-gonic/gin"
)

type LocationController interface {
	SyncLocation(context *gin.Context)
}
