package controller

import (
	"net/http"

	"gitlab.com/VNEU/sync-visit-ski/model/web"
	"gitlab.com/VNEU/sync-visit-ski/service"

	"github.com/gin-gonic/gin"
)

type CustomerControllerImpl struct {
	CustomerService service.CustomerService
}

func NewCustomerController(customerService service.CustomerService) CustomerController {
	return &CustomerControllerImpl{
		CustomerService: customerService,
	}
}

func (controller *CustomerControllerImpl) SyncCustomer(c *gin.Context) {
	customer := c.Param("isAll")
	controller.CustomerService.SyncCustomer(customer, c)
	webResponse := web.WebResponse{
		Success: true,
		Message: "Sync Customer Succesfully",
	}

	c.JSON(http.StatusOK, webResponse)
}
