package controller

import (
	"net/http"

	"gitlab.com/VNEU/sync-visit-ski/model/web"
	"gitlab.com/VNEU/sync-visit-ski/service"

	"github.com/gin-gonic/gin"
)

type LocationControllerImpl struct {
	LocationService service.LocationService
}

func NewLocationController(LocationService service.LocationService) LocationController {
	return &LocationControllerImpl{
		LocationService: LocationService,
	}
}

func (controller *LocationControllerImpl) SyncLocation(c *gin.Context) {
	location := c.Param("isAll")
	controller.LocationService.SyncLocation(location, c)
	webResponse := web.WebResponse{
		Success: true,
		Message: "Sync Location Succesfully",
	}

	c.JSON(http.StatusOK, webResponse)
}
