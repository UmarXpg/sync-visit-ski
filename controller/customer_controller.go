package controller

import (
	"github.com/gin-gonic/gin"
)

type CustomerController interface {
	SyncCustomer(context *gin.Context)
}
