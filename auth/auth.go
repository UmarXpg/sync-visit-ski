package auth

import (
	"fmt"
	"net/http"
	"os"
	"strconv"
	"strings"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"gitlab.com/VNEU/sync-visit-ski/helper"
	"gitlab.com/vneu/go-helper/exception"
)

type CreateAuthFunc func(userID string, tokenDetails *TokenDetails)

type AccessDetails struct {
	UserID                     uint
	CompanyID                  uint
	StructureID                string
	MandatorySurvey            bool
	CheckPointVisitSchedule    bool
	CheckPointVisitRealization bool
	StructureSubordinates      string
}

type TokenDetails struct {
	AccessToken         string
	RefreshToken        string
	AccessUUID          string
	RefreshUUID         string
	UserAgent           string
	RemoteAddress       string
	AtExpired           int64
	RefreshTokenExpired int64
}

func Auth(next func(c *gin.Context, auth *AccessDetails), roles []string) gin.HandlerFunc {
	return func(c *gin.Context) {
		// Check JWT Token
		tokenAuth, err := ExtractTokenMetadata(ExtractToken(c.Request))
		if err != nil {
			helper.PanicIfError(exception.ErrUnauthorized)
		}

		// Check Permission User
		// if !helper.Contains(roles, tokenAuth.Role) {
		// 	helper.PanicIfError(exception.ErrPermissionDenied)
		// }

		next(c, tokenAuth)
	}
}

func ExtractTokenMetadata(tokenString string) (*AccessDetails, error) {
	token, err := VerifyToken(tokenString)
	if err != nil {
		return nil, err
	}

	claims, ok := token.Claims.(jwt.MapClaims)
	if ok {
		companyID, err := strconv.ParseUint(fmt.Sprintf("%.f", claims["company_id"]), 10, 64)
		if err != nil {
			return nil, err
		}

		userID, err := strconv.ParseUint(fmt.Sprintf("%.f", claims["id"]), 10, 64)
		if err != nil {
			return nil, err
		}
		return &AccessDetails{
			UserID:                     uint(userID),
			StructureID:                claims["structure_id"].(string),
			StructureSubordinates:      claims["structure_subordinates"].(string),
			CompanyID:                  uint(companyID),
			MandatorySurvey:            claims["mandatory_survey"].(bool),
			CheckPointVisitSchedule:    claims["check_point_visit_schedule"].(bool),
			CheckPointVisitRealization: claims["check_point_visit_realization"].(bool),
		}, nil
	}

	return nil, err
}

func VerifyToken(stringToken string) (*jwt.Token, error) {
	token, _ := jwt.Parse(stringToken, func(t *jwt.Token) (interface{}, error) {
		return []byte(os.Getenv("ACCESS_SECRET")), nil
	})

	return token, nil
}

func ExtractToken(r *http.Request) string {
	bearToken := r.Header.Get("Authorization")
	strArr := strings.Split(bearToken, " ")
	if len(strArr) == 2 {
		return strArr[1]
	}
	return ""
}
