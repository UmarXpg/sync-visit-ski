package helper

func ConvertLevelStructureSkiToVisit(level string) int {
	var levelVisit int
	switch level {
	case "MR":
		levelVisit = 1
	case "SPV":
		levelVisit = 2
	case "ASM":
		levelVisit = 3
	case "FSM":
		levelVisit = 4
	case "GM":
		levelVisit = 5
	case "MD":
		levelVisit = 6
	}
	return levelVisit
}
