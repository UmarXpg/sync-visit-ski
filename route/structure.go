package route

import (
	"gitlab.com/VNEU/sync-visit-ski/controller"
	"gitlab.com/VNEU/sync-visit-ski/repository"
	"gitlab.com/VNEU/sync-visit-ski/service"
	structureVisit "gitlab.com/VNEU/visit-flow-go/repository"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"gorm.io/gorm"
)

func StructureRoute(router *gin.Engine, db *gorm.DB, validate *validator.Validate) {

	structureService := service.NewStructureService(
		repository.NewStructureRepository(),
		structureVisit.NewStructureRepository(),
		db,
		validate,
	)
	structureController := controller.NewStructureController(structureService)

	router.POST("/sync-structures/:isAll", structureController.SyncStructure)
}
