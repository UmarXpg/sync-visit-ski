package route

import (
	"gitlab.com/VNEU/sync-visit-ski/controller"
	"gitlab.com/VNEU/sync-visit-ski/repository"
	"gitlab.com/VNEU/sync-visit-ski/service"
	locationVisit "gitlab.com/VNEU/visit-flow-go/repository"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"gorm.io/gorm"
)

func LocationRoute(router *gin.Engine, db *gorm.DB, validate *validator.Validate) {

	locationService := service.NewLocationService(
		repository.NewLocationRepository(),
		locationVisit.NewLocationRepository(),
		db,
		validate,
	)
	locationController := controller.NewLocationController(locationService)

	router.POST("/sync-locations/:isAll", locationController.SyncLocation)
}
