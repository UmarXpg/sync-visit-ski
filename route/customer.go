package route

import (
	"gitlab.com/VNEU/sync-visit-ski/controller"
	"gitlab.com/VNEU/sync-visit-ski/repository"
	"gitlab.com/VNEU/sync-visit-ski/service"
	customerVisit "gitlab.com/VNEU/visit-flow-go/repository"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"gorm.io/gorm"
)

func CustomerRoute(router *gin.Engine, db *gorm.DB, validate *validator.Validate) {

	customerService := service.NewCustomerService(
		repository.NewCustomerRepository(),
		customerVisit.NewCustomerRepository(),
		db,
		validate,
	)
	customerController := controller.NewCustomerController(customerService)

	router.POST("/sync-customers/:isAll", customerController.SyncCustomer)
}
